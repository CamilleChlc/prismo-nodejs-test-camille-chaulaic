export interface Member {
id: number;
firstname: string;
lastname: string;
avatar: string;
function: string;
description: string;
}